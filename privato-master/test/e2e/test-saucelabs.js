/* eslint-disable */

'use strict'

import Page from './page';
import { Selector, ClientFunction } from 'testcafe';

let email = 'cchulotest@test.com'
let password = 'CSE112Rocks!?'

const page = new Page();

fixture `E2E Test`
    .page `https://privato-cse112.appspot.com`;

test('Can log in to site', async t => {

    const getLocation = ClientFunction(() => document.location.href.toString())

    await t
        .typeText(page.emailInput, email)
        .typeText(page.pwInput, password)
        .click(page.loginBtn)
        .expect(getLocation()).contains('feed')
    
});

// TEST TESTS WILL NOT WORK BECAUSE SAUCE LABS BROWSER VM DOES NOT HAVE ACCESSIBLE LOCALSTORE

// test('Can make a post', async t => {

//     const getLocation = ClientFunction(() => document.location.href.toString())
//     const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890"
//     let message = ''
//     for (let i = 0; i < 5; i++) {
//         message += possible.charAt(Math.floor(Math.random() * possible.length))
//     }

//     const postsNum = Selector('#post').count
//     const expansionPost = Selector('#feed-expansion')
//     const postText = Selector('#feed-post')
//     const buttonPost = Selector('#feed-btn-post')
//     const post = Selector('#post')

//     await t
//         .expect(getLocation()).contains('feed')
//         .click(expansionPost)
//         .wait(1000)
//         .click(buttonPost)
//         .expect(post).eql(postsNum + 1)
    
// });

// test('Can make a search for a user', async t => {

//     const getLocation = ClientFunction(() => document.location.href.toString())

//     await t
//         .expect(getLocation()).contains('feed')
    
// });

test('Can make logout', async t => {

    const getLocation = ClientFunction(() => document.location.href.toString())

    await t
        .expect(getLocation()).contains('feed')
        .click(Selector('#feed-settings'))
        .expect(getLocation()).contains('Settings')
        .click(Selector('#settings-logout'))
        .expect(getLocation()).contains('login')
});