/* eslint-disable */

import { Selector } from 'testcafe';

export default class Page {

    emailInput;
    pwInput;
    loginBtn;

    

    constructor () {
        this.emailInput = Selector('#login-email');
        this.pwInput = Selector('#login-pw');
        this.loginBtn = Selector('#login-btn');
    }
}