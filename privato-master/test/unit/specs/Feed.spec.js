import Feed from '@/components/Feed'
import { mount } from 'avoriaz'
import { assert } from 'chai'

let FeedComponent
let methods
let makePostHelper
let deletePostHelper
let update
const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
let uuid = {user: {uuid: 'ns9bXvSPHKQhWRh9UnA2hhnMRqh1'}}
let postid

describe('Feed.vue', () => {
  before(() => {
    FeedComponent = mount(Feed)
    methods = FeedComponent.methods()
    makePostHelper = methods.makePostHelper
    deletePostHelper = methods.deletePostHelper
    update = methods.update
    localStorage.setItem('userProfile', JSON.stringify(uuid))
  })

  it('posts, makePostHelper and update should be defined', () => {
    expect(typeof FeedComponent.methods).to.not.equal('undefined')
    expect(typeof makePostHelper).to.equal('function')
    expect(typeof deletePostHelper).to.equal('function')
    expect(typeof update).to.equal('function')
    expect(typeof FeedComponent.data().posts).to.not.equal('undefined')
  })

  it('update works', () => {
    update().then(data => {
      assert(true)
    }).catch(() => {
      assert(false)
    })
  })

  it('makePostHelper work', () => {
    makePostHelper('this is a test',
      'this is a test',
      uuid.user.uuid,
      new Date().getTime()).then(result => {
      deletePostHelper(result).then(result => {
        if (result) assert(result)
        else assert(false)
      }).catch(() => {
        assert(false)
      })
    }).catch(() => {
      assert(false)
    })
  })

  it('update can pull newly posted message', () => {
    let name = ''
    let message = ''

    for (let i = 0; i < 5; i++) {
      name += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    for (let i = 0; i < 5; i++) {
      message += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    makePostHelper(name,
      message,
      uuid.user.uuid,
      new Date().getTime()).then(id => {
      postid = id
      update().then(data => {
        for (let i = 0; i < data.length; i++) {
          if (data[i].id === postid) {
            expect(data[i].data.message).to.equal(message)
            expect(data[i].data.name).to.equal(name)
          } else {
            assert(false)
          }
        }
      })
    }).catch(() => {
      assert(false)
    })
  })

  it('post from last test should be deleted', () => {
    deletePostHelper(postid).then(shouldBeTrue => {
      assert(shouldBeTrue)
    }).catch(() => {
      assert(false)
    })
  })
})
