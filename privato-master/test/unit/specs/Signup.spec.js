import Signup from '@/components/Signup'
import firebase from 'firebase'
import { mount } from 'avoriaz'
import { assert } from 'chai'

let email = 'something@somethingrandom.com'
let password = 'somethingsomethingpassword'

describe('Signup.vue', () => {
  before(() => {
    // Initialize Firebase
    var config = {
      apiKey: 'AIzaSyCn7FDf91xj39Df-Q3EuvYKY-K0jRs8s38',
      authDomain: 'privato-cse112.firebaseapp.com',
      databaseURL: 'https://privato-cse112.firebaseio.com',
      projectId: 'privato-cse112',
      storageBucket: 'privato-cse112.appspot.com',
      messagingSenderId: '1098046336096'
    }

    firebase.initializeApp(config)
  })

  after(async function () {
    this.timeout(10000)
    await firebase.auth().signInWithEmailAndPassword(email, password)
    await firebase.auth().currentUser.delete()
  })

  it('createAccount should be defined', () => {
    const SignupComponent = mount(Signup)
    expect(typeof SignupComponent.methods).to.not.equal('undefined')
    let methods = SignupComponent.methods()
    const createAccount = methods.createAccount
    expect(typeof createAccount).to.equal('function')
  })

  it('can create an account', async function () {
    this.timeout(5000)
    const SignupComponent = mount(Signup)
    let methods = SignupComponent.methods()
    const createAccount = methods.createAccount
    let result = await createAccount(email, password)
    if (result.result !== undefined) {
      assert(true)
    } else {
      assert(false)
    }
  })
})
