import Login from '@/components/Login'
import firebase from 'firebase'
import { mount } from 'avoriaz'
import { assert } from 'chai'

let email = 'cchulotest@test.com'
let password = 'CSE112Rocks!?'
let uuid = {user: {uuid: 'ns9bXvSPHKQhWRh9UnA2hhnMRqh1'}}

describe('Login.vue', () => {
  it('firebaseSignIn and getUserProfile should be defined', () => {
    const LoginComponent = mount(Login)
    expect(typeof LoginComponent.methods).to.not.equal('undefined')
    let methods = LoginComponent.methods()
    const fireBaseSignIn = methods.fireBaseSignIn
    const getUserProfile = methods.getUserProfile
    expect(typeof fireBaseSignIn).to.equal('function')
    expect(typeof getUserProfile).to.equal('function')
  })

  it('firebaseSign can sign user in', async () => {
    const LoginComponent = mount(Login)
    let methods = LoginComponent.methods()
    const fireBaseSignIn = methods.fireBaseSignIn
    fireBaseSignIn(email, password).then(() => {
      firebase.auth().signOut().then(() => {
        assert(true)
      }).catch(() => {
        assert(false)
      })
    }).catch(() => {
      assert(false)
    })
  })

  it('can get userprofile', async () => {
    const LoginComponent = mount(Login)
    let methods = LoginComponent.methods()
    const getUserProfile = methods.getUserProfile
    getUserProfile(uuid).then(userProfile => {
      expect(userProfile.data.email).to.equal(email)
    }).catch(() => {
      assert(false)
    })
  })

  it('firebaseSignIn and getUserProfile play nice', () => {
    const LoginComponent = mount(Login)
    let methods = LoginComponent.methods()
    const getUserProfile = methods.getUserProfile
    const fireBaseSignIn = methods.fireBaseSignIn
    fireBaseSignIn(email, password).then(user => {
      getUserProfile(user).then(userProfile => {
        expect(userProfile.data.email).to.equal(email)
        expect(userProfile.data.uuid).to.equal(uuid)
      }).catch(() => {
        assert(false)
      })
    }).catch(() => {
      assert(false)
    })
  })
})
