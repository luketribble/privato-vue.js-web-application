'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const admin = require('firebase-admin');
const functions = require('firebase-functions');

const serviceAccount = require('./privato.json');
const _ = require('underscore');

const config = {
    apiKey: "AIzaSyCn7FDf91xj39Df-Q3EuvYKY-K0jRs8s38",
    authDomain: "privato-cse112.firebaseapp.com",
    databaseURL: "https://privato-cse112.firebaseio.com",
    projectId: "privato-cse112",
    storageBucket: "privato-cse112.appspot.com",
    messagingSenderId: "1098046336096",
    credential: admin.credential.cert(serviceAccount)
};



admin.initializeApp(config);

let db = admin.firestore();

// likes, comments, posts, users

/*
 * npm install [packagename] : installs module but does not save it in
 *  package.json under dependencies
 * npm install --save [packagename] : installs module AND saves it in
 *  package.json
 * npm install : installs all packages listed in package.json
 */

/** @module app */
/**
 * Automates setting up node
 * @const
 */
const app = express();

/**
 * Express router that sets up backend routes
 * @const
 */
const router = express.Router();

console.log(process.env.NODE_ENV);

/** REST API: GET, POST, PUT, DELETE */

if (process.env.NODE_ENV === 'production') {
    app.use('/static', express.static(path.join(__dirname, 'static')));
}

app.get('/service-worker.js', (request, response) => {
    if(process.env.NODE_ENV !== 'development'){
        response.sendFile(path.resolve(__dirname + '/service-worker.js'));
    } else {
        response.status(200);
    }
});

app.get('/app.js', (request, response, next) => {
    console.log(request.query);
    let file = request.query['_sw-precache'];
    response.sendFile(path.resolve(__dirname, 'static', 'js', 'app.' + file + '.js'));
    // next();
})
 
/**
 * GET call on path '/', and fire function that is defined in the second
 * parameter
 * @function get
 * @param {Object} req - request
 * @param {Object} res - response
 */
app.get('/', (request, response) => {
    if (process.env.NODE_ENV !== 'development') {
        // console.log(__dirname);
        response.sendFile(__dirname + '/index.html');
    } else {
        response.status(200);
    }

});

/**
 * Helper function for sorting according to time stamps
 * @function sortHelper
 * @param {Object} a - first object to compare 
 * @param {Object} b - second object to compare with
 * @memberof module:posts
 */
function sortHelper(a, b){
    if(a.data.timestamp < b.data.timestamp) return 1;
    if(a.data.timestamp > b.data.timestamp) return -1;
    return 0;
}


/** restAPI for posts (in feed)
 * @module posts 
 * @name posts
 */
// restAPI for posts (in feed?)
router.route('/posts/:uid?')
   /**
    * Retrieves posts from the database for the feed. (GET request)
    * It first gets all the posts and then filters out only the posts that were
    * posted by the current user's friends.
    * @function get
    * @param {Object} request - request
    * @param {Object} response - response
    */
    .get(async (request, response) => {
        
        let uid = request.params.uid;
        
        //fixes uid bug
        if(uid === undefined) {
            response.status(200).json([]);
            return;
        }

        let postsQuery = await db.collection('posts').get();

        let userQuery = await db.collection('users').where('uid', '==', uid).get();

        let friendQuery = await db.collection('users').where('friends.' + uid, '==', true).get();

        let posts = []
        
        postsQuery.forEach(post => {
            posts.push({id: post.id, data: post.data()});
        })

        let friendsOfCurrUser = []
        userQuery.forEach(user => {
            friendsOfCurrUser = user.data().friends;
        })

        let friends = {}
        
        friendQuery.forEach(friend => {
            let data = friend.data();
            if(data.friends[uid]){
                friends[data.uid] = true;
            }
        })

        let processedPosts = [];

        posts.forEach(post => {
            if(post.data.uid === uid || (friends[post.data.uid] && friendsOfCurrUser[post.data.uid]))
                processedPosts.push(post);
        })
        response.status(200).json(processedPosts.sort(sortHelper));
        
    })
   /**
    * Adds/Stores posts to the database. (POST request)
    * @function post
    * @param {Object} request - request
    * @param {Object} response - response
    */
    .post((request, response) => {
        var setDoc = db.collection('posts').add(request.body).then(ref => {
            console.log('Added post with ID: ', ref.id);
        });

        response.status(200).json(request.body);
    })
   /**
    * Edits/Updates posts on the database. (PUT request)
    * @function put
    * @param {Object} request - request
    * @param {Object} response - response
    */
    .put((request, response) => {
        // Need to establish editing components first
        // var data = request.body;

        //var setDoc = db.collection('posts').set(data);
        response.status(200).json("NOT YET IMPLEMENTED");
    })
   /**
    * Deletes posts from the database. (DELETE request)
    * @function delete
    * @param {Object} request - request
    * @param {Object} response - response
    */
    .delete((request, response) => {
        let deleteDoc = db.collection('posts').doc(request.body.id).then(() =>
            response.status(200).json(true)
        ).catch(() => response.status(500).json(false));
        
    })


/** restAPI for user posts
 * @module userPosts
 * @name userPosts
 */
// restAPI for user posts
router.route('/users/:uid')
   /**
    * Retrieves user posts from the database. (GET request)
    * @function get
    * @param {Object} request - request
    * @param {Object} response - response
    */
    .get((request, response) => {
        let uid = request.params.uid;

        db.collection('users').get().then(users => {
            let processedUsers = [];
            users.forEach(user => {
                processedUsers.push(user.data());
            })
            let user = _.findWhere(processedUsers, { uid: uid });
            response.status(200).json(user);
        }).catch(err => {
            response.status(500).json(err);
        });

    })
   /**
    * Adds/Stores user posts to the database. (POST request)
    * @function post
    * @param {Object} request - request
    * @param {Object} response - response
    */
    .post((request, response) => {
        // Structure will be identical to posts, need to add functionality + get more clarity for this route though
        var setDoc = db.collection('users').add(request.body).then(ref => {
            console.log('Added user with ID: ', ref.id);
            // request.body contains user profile { username, email, uid }
            response.status(200).json(request.body);
        }).catch(err => {
            response.status(500).json(err);
        });


    })
   /**
    * Edits/Updates user posts on the database. (PUT request)
    * @function put
    * @param {Object} request - request
    * @param {Object} response - response
    */
    .put((request, response) => {
        
        let uid = request.params.uid;
        console.log(request.body)
        db.collection('users').where('uid', '==', uid).get().then(snapshot => {
            //console.log(snapshot)
            snapshot.forEach(doc => {
                //console.log(doc.id, "=>", doc.data())
                let userId = doc.id;
                //console.log('user found', id);
                db.collection('users').doc(userId).set(request.body).then(() => {
                    response.status(200).json(request.body);
                }).catch(err => {
                    console.error(err);
                    response.status(500).status(err);
                })
            })
        }).catch(err => {
            console.error(err);
            response.status(500).json(err);
        });

        
    })
    /**
    * Deletes user posts from the database. (DELETE request)
    * @function delete
    * @param {Object} request - request
    * @param {Object} response - response
    */
    .delete((request, response) => {
        var deleteDoc = db.collection('users').doc(ref.id);
        response.status(200).json("NOT YET IMPLEMENTED");
    })

/** restAPI for emails
 * @module email
 * @name email
 */
router.route('/useremail/:email?')
    /**
    * Retrieves emails from the database. (GET request)
    * @function get
    * @param {Object} request - request
    * @param {Object} response - response
    */
    .get((request, response) => {

        let email = request.params.email;
        console.log(email);
        db.collection('users').where('email', '==', email).get()
            .then(snapshot => {
                snapshot.forEach(doc => {
                    response.status(200).json([doc.data()])
                    return;
                });
            }).catch(err => {
                response.status(500).json([])
            });
    })

        
    
// https://medium.com/@adamzerner/how-bodyparser-works-247897a93b90
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// tells express what the root of route
app.use('/api', router);


/**
 * Helper function to start server
 * @memberof module:app
 * @function
 */
function runServer(customport) {

    const port = process.env.PORT || customport;

    return new Promise((resolve, reject) => {

        var server = app.listen(port, () => {

            console.log(`Example app is listening on port ${port}`);
            resolve(server);

        }).on('error', err => {

            reject(err);

        });
    });
}

/**
 * Closes the server
 * @memberof module:app
 * @function
 */
function closeServer(server) {
    return new Promise((resolve, reject) => {
        console.log('Closing server');
        server.close(err => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        });
    });
}

if (require.main === module) {
    runServer(3000);
}

module.exports = { app, runServer, closeServer };
