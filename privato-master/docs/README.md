Welcome to the documentation page for Privato! We are using JSDoc to generate
our documents.

## How to Run JSDoc
For now, this is how to run JSDoc using DocStrap as a template:

`jsdoc [files] -t ./node_modules/ink-docstrap/template -c ./docs/conf.json -d docs -R docs/README.md`

Or, you can also add the path to the file in docs/conf.json under "source", and
under "include"

Or, just use the npm script: 

`npm run docs`



## Useful Links

How to Use JSDoc:

<http://usejsdoc.org/>

DocStrap (and themes):

<https://www.npmjs.com/package/ink-docstrap>

Pros/Cons of JSDoc and other Documentaion Generators:

<https://www.fusioncharts.com/blog/jsdoc-vs-yuidoc-vs-doxx-vs-docco-choosing-a-javascript-documentation-generator/>
