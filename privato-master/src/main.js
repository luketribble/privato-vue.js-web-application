// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase/'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
Vue.use(Vuetify)

Vue.config.productionTip = false

let app

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyCn7FDf91xj39Df-Q3EuvYKY-K0jRs8s38',
  authDomain: 'privato-cse112.firebaseapp.com',
  databaseURL: 'https://privato-cse112.firebaseio.com',
  projectId: 'privato-cse112',
  storageBucket: 'privato-cse112.appspot.com',
  messagingSenderId: '1098046336096'
}

firebase.initializeApp(config)
firebase.auth().onAuthStateChanged(function (user) {
  if (!app) {
    /* eslint-disable no-new */
    new Vue({
      el: '#app',
      router,
      components: { App },
      template: '<App/>'
    })
  }
})
