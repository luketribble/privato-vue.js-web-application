import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/components/Login'
import Signup from '@/components/Signup'
import Feed from '@/components/Feed'
import CreatePost from '@/components/CreatePost'
import ViewPost from '@/components/ViewPost'
import Settings from '@/components/Settings'
import Upgrade from '@/components/Upgrade'
import Search from '@/components/Search'
import Notifications from '@/components/Notifications'

import firebase from 'firebase'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '*',
      name: 'Login',
      component: Login
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/feed',
      name: 'Feed',
      component: Feed,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/search',
      name: 'Search',
      component: Search,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/create-post',
      name: 'CreatePost',
      component: CreatePost,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/view-post',
      name: 'ViewPost',
      component: ViewPost,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/Notifications',
      name: 'Notifications',
      component: Notifications,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/upgrade',
      name: 'Upgrade',
      component: Upgrade,
      meta: {
        requiresAuth: true
      }
    }

  ]
})

router.beforeEach((to, from, next) => {
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  if (requiresAuth && !currentUser) next('login')
  else next()
})

export default router
